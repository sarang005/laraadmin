<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Carbon\Carbon;
use Illuminate\Foundation\Auth;
use Illuminate\Http\Request;
use App\Authentication_Log;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/userlist';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    function authenticated(Request $request, $user)
    {
        $authentication_log = new Authentication_Log;
        $authentication_log->user_id = $user->id;
        $authentication_log->login_time = Carbon::now('UTC');
        $authentication_log->user_agent = $request->header('User-Agent');
        $authentication_log->user_ip = $request->ip();
        $authentication_log->save();

        $value=$request->session()->put('id',$authentication_log->id);
    }
}
