<?php

namespace App\Http\Controllers;

use App\Rules;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserRequest;
use App\Services\UserService;
use Excel;
use App\Exports;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Jobs\ExportExcel;

class UserController
{
    protected $userservice;

    public function __construct(UserService $userservice)
    {
        $this->userservice = $userservice;
    }

    public function index()
    {

        $count=1;
        $users = $this->userservice->index();

        return view('User.userlist', compact('users','count'));
    }

    public function create(UserRequest $request)
    {
      $this->userservice->create($request);

      return back()->with(['status'=>'Post created successfully']);
    }

    public function read($id)
    {
       $users = $this->userservice->read($id);

       return view('User/edit', compact('users'));
    }

    public function update(Request $request, $id)
    {

        $users = $this->userservice->update($request, $id);

        return redirect()->route('User.userlist')->with(['status'=>'Updated Succesfully!!!']);
    }

    public function delete($id)
    {
        $this->userservice->delete($id);

        return back()->with(['status'=>'Deleted successfully']);
    }

    public function export()
    {
        //ExportExcel::dispatch()->onQueue('jobs');

        ExportExcel::dispatch();
        //$this->dispatch(new ExportExcel($user));
        $this->userservice->export();


    }

    public function profile()
    {
        $id = $this->userservice->profile();
        return view('User.profile', compact('id'));
    }

}




