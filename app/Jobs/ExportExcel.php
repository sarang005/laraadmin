<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


use Mail;
use App\User;

class ExportExcel implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data=array('data' =>'Requested has been received successfully');

        Mail::send('User.mail', $data, function($message){
            $message->from('sarangbelsare0005@gmail.com', 'Export Excel Queues');
            $message->to('sarangbelsare0005@gmail.com')->subject('There is a new');
            });
    }
}
