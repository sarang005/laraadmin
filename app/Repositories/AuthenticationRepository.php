<?php

namespace App\Repositories;

use App\Post;

class AuthenticationRepository
{

  protected $authentication;

  public function __construct(Authentication $authentication)
  {
    $this->authentication = $authentication;
  }

  public function all()
  {
    return $this->authentication->all();
  }
}
