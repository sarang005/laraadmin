<?php

namespace App\Repositories;

use App\Rules;
use App\User;
use App\UserActivity;

class UserActivityRepository
{
    protected $useractivity;
    private $userRepository;

    public function __construct(UserActivity $useractivity, UserRepository $userRepository)
    {
        $this->useractivity = $useractivity;
        $this->userRepository = $userRepository;
    }

    public function create($attributes)
    {
        return $this->useractivity->create($attributes);
    }

    public function all()
    {
        return $this->useractivity->all();
    }

    public function find($id)
    {
        return $this->useractivity->find($id);
    }

    public function update($id, array $attributes)
    {
        return $this->useractivity->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->useractivity->find($id)->delete();
    }

    public function insert($attributes)
  {
      return $this->useractivity->insert($attributes);
  }
}
