<?php

namespace App\Repositories;

use App\User;
use App\Exports\UsersExport;
use DB;
use Excel;
use App\Mail;
use App\Jobs;
use App\Mail\SendEmailTest;

class UserRepository
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function create($attributes)
    {
        return $this->user->create($attributes);
    }

    public function all()
    {
        return $this->user->orderBy('id', 'DESC')->paginate(10);

    }

    public function find($id)
    {
        return $this->user->find($id);
    }

    public function update($id, array $attributes)
    {
        $data=$this->user->all();

        return $this->user->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->user->find($id)->delete();
    }

    public function export()
    {
        $User_data= DB::table('users')->get()->toArray();
        $user_array[]=array('first_name','last_name','email','address','created_at');
        foreach ($User_data as $user) {
                $user_array[]= array(
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'email' =>  $user->email,
                    'address' =>  $user->address,
                    'created_at'=> $user->created_at
                );
        }

        Excel::create('User Data', function($excel) use ($user_array){
            $excel->setTitle('User Data');
            $excel->sheet('User Data', function($sheet) use ($user_array){
             $sheet->fromArray($user_array, null, 'A1', false, false);
            });
           })->download('xlsx');
    }

    public function profile()
    {
        $id = User::find(auth()->id());
        return $id;
    }




}
