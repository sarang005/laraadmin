<?php

namespace App\Services;

use App\Post;
use App\Repositories\AuthenticationRepository;
use Illuminate\Http\Request;

class AuthenticationService
{
    public function __construct(AuthenticationRepository $authentication)
    {
        $this->authentication = $authentication ;
    }

    public function create(Request $request)
    {
        $attributes = $request->all();

        return $this->authentication->create($attributes);
    }
}
