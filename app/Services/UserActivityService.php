<?php

namespace App\Services;

use App\Rules;
use App\Services\UserService;
use App\UserActivity;
use App\Repositories\UserActivityRepository;
use Illuminate\Http\Request;
use Carbon\Carbon;

class UserActivityService
{

    private $useractivity;

    public function __construct(UserActivityRepository $useractivity,UserActivityRepository $userActivityRepository)
    {
        $this->useractivity = $useractivity ;
        $this->userActivityRepository = $userActivityRepository ;
    }

    public function index()
    {
        return $this->useractivity->all();
    }

    public function create(Request $request)
    {
        $attributes = $request->all();

        return $this->useractivity->create($attributes);
    }

    public function read($id)
    {
        return $this->useractivity->find($id);
    }

    public function update(Request $request, $id)
    {

        return $this->useractivity->update($id, $attributes);
    }

    public function delete($id)
    {
        return $this->useractivity->delete($id);
    }

    public function trackactivity($oldUserData,$newUserData)
    {
        /**
         * user_id
         * field_name
         * old_value
         * modified_valueUserAc
         * modified_by
         */

        $oldUserData = $oldUserData->toArray();
        $newUserData = $newUserData->toArray();

        $updatedData = array_diff($newUserData, $oldUserData);

        $trackableFields = config('constants.FIELDS_TO_TRACK');

        $trackActivityData = [];

        foreach ($trackableFields as $key => $field) {

            if (isset($updatedData[$field])) {
                $valuesToInsert = [
                    'user_id' => $oldUserData['id'],
                    'field_name' => $field,
                    'old_value' => $oldUserData[$field],
                    'modified_value' => $newUserData[$field],
                    'modified_by' => auth()->id(),
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now(),
                ];

                $trackActivityData[] = $valuesToInsert;
            }
        }

        if (!empty($trackActivityData)) {
            $this->userActivityRepository->insert($trackActivityData);
        }
    }
}
