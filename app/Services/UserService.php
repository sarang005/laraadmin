<?php

namespace App\Services;

use App\Services\UserActivityService;
use App\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Rule;
use App\Rules\ValidEmail;

class UserService
{
    private $userActivityService;
    private $userRepository;

    public function __construct(UserRepository $userRepository, UserActivityService $userActivityService)
    {
        $this->userRepository = $userRepository;
        $this->userActivityService = $userActivityService;
    }

    public function index()
    {
        return $this->userRepository->all();
    }

    public function create(Request $request)
    {
        $attributes = $request->all();

        return $this->userRepository->create($attributes);
    }

    public function read($id)
    {
        return $this->userRepository->find($id);
    }

    public function update(Request $request, $id)
    {

        $attributes = $request->all();

        $oldUserData = User::find($id);

        $result = $this->userRepository->update($id, $attributes);

        $modifiedValues = $this->userRepository->find($id);

        if ($result)
        {
            return $this->userActivityService->trackactivity($oldUserData,$modifiedValues);
        }

        return false;
    }

    public function delete($id)
    {
        return $this->userRepository->delete($id);
    }

    public function export()
    {
        $data=$this->userRepository->export();
    }

    public function profile()
    {
        return $this->userRepository->profile();
    }

}
