@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit') }}</div>

                <div class="card-body">
                    <form method="post" action="/users/update/{{ $users->id }}">
                    @csrf
                        <div class="form-group">
                            <label for="exampleFormControlInput1">First Name</label>
                            <input type="text" name="first_name" class="form-control" id="exampleFormControlInput1" value="{{ $users->first_name }}" >
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Last Name</label>
                            <input type="text" name="last_name" class="form-control" id="exampleFormControlInput1" value="{{ $users->last_name }}" >
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect2">Email</label>
                            <input type="email" name="email" class="form-control" id="exampleFormControlInput1" value="{{ $users->email }}" >
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Mobile</label>
                            <input type="text" name="mobile" class="form-control" id="exampleFormControlInput1" value="{{ $users->mobile }}" >
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Address</label>
                            <input type="text" name="address" class="form-control" id="exampleFormControlInput1" value="{{ $users->address }}" >
                        </div>
                         <button class="btn btn-primary"  type="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
