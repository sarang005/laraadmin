@extends('layouts.app')

@section('content')

<div class="container">
      <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action active">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Your Profile</h5>
                </div>
            </a>
            <a href="#" class="list-group-item list-group-item-action">
              <div class="d-flex w-100 justify-content-between">
                    <span class="text-muted">Name</span>
                    <h5 class="mb-1">{{ $id->first_name }} {{ $id->last_name }}</h5>
                    <small class="text-muted"></small>
              </div>
            </a>
            <a href="#" class="list-group-item list-group-item-action">
              <div class="d-flex w-100 justify-content-between">
                    <span class="text-muted">email</span>
                    <h5 class="mb-1">{{ $id->email }}</h5>
                    <small class="text-muted"></small>
              </div>
            </a>
            <a href="#" class="list-group-item list-group-item-action">
                <div class="d-flex w-100 justify-content-between">
                    <span class="text-muted">Mobile</span>
                    <h5 class="mb-1">{{ $id->mobile }}</h5>
                    <small class="text-muted"></small>
                </div>
            </a>
        </div>
</div>
@endsection

