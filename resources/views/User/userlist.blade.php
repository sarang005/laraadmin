    @extends('layouts.app')

    @section('content')
<div class="container-fluid">
    <div class="row justify-content-center">

        <div class="col-md-12">

            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body" >
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-hover">
                        <thead>
                            <tr class="text-justify">
                                <th scope="col">id</th>
                                <th scope="col">FirstName</th>
                                <th scope="col">LastName</th>
                                <th scope="col">Email</th>
                                <th scope="col">Address</th>
                                <th scope="col">Mobile</th>
                                <th scope="col">Created_At</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>

                        @foreach ($users as $user)

                        <tbody>
                            <tr>
                                <td>{{ $count++}}</td>
                                <td>{{ $user->first_name }}</td>
                                <td>{{ $user->last_name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->address }}</td>
                                <td>{{ $user->mobile }}</th>
                                <td>{{ $user->created_at->toDateString() }}</td>
                                <td>
                                <a href="userlist/{{$user->id}}/edit" class="btn btn-primary">Edit</a>
                                <a href="userlist/{{$user->id}}" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                    <div class="d-flex justify-content-center">{{ $users->links() }}</div>
                </div>
                <div class="row">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Showing {{($users->currentpage()-1)*$users->perpage()+1}} to {{$users->currentpage()*$users->perpage()}}
                        of  {{$users->total()}} entries

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
