<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify'=>true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/userlist', 'UserController@index')->name('User.userlist');
Route::get('/userlist/{user}/edit','UserController@read');
Route::get('/userlist/{user}','UserController@delete');
Route::post('/users/update/{id}', 'UserController@update');
Route::get('/Export','UserController@Export');
Route::get('/userprofile','UserController@profile');
Route::get('email', 'EmailController@sendEmail');
